// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS/Animation/TPSAnimNotify.h"

void UTPSAnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	OnNotified.Broadcast(MeshComp);

	Super::Notify(MeshComp, Animation);
}