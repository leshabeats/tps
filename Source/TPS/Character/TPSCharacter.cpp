// Copyright Epic Games, Inc. All Rights Reserved.

#include "TPSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "TPS/Game/TPSGameInstance.h"
#include "TPS/Weapons/Projectiles/ProjectileDefault.h"
#include "TPS/Character/TPSCharacterHealthComponent.h"
#include "TPS/TPS.h"
#include "TPS/Animation/TPSEquipStartAnimNotify.h"
#include "Net/UnrealNetwork.h"
#include "TPS/Animation/TPSAnimUtils.h"
#include "Engine/ActorChannel.h"

DEFINE_LOG_CATEGORY_STATIC(LogTPSCharacter, All, All);

ATPSCharacter::ATPSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f; 
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a inventory and health component
	InventoryComponent = CreateDefaultSubobject<UTPSInventoryComponent>(TEXT("InventoryComponent"));
	HealthComponent = CreateDefaultSubobject<UTPSCharacterHealthComponent>(TEXT("HealthComponent"));

	

	if (HealthComponent)
	{
		HealthComponent->OnDead.AddDynamic(this, &ATPSCharacter::CharDead);
	}

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	//Network
	bReplicates = true;
}

void ATPSCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC && myPC->IsLocalPlayerController())
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(FVector(TraceHitResult.ImpactNormal).Rotation());
		}
	}

	MovementTick(DeltaSeconds);
	AbilityTick(DeltaSeconds);
}

void ATPSCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (GetWorld() && GetWorld()->GetNetMode() != NM_DedicatedServer)
	{
		if (CursorMaterial && GetLocalRole() == ROLE_AutonomousProxy || GetLocalRole() == ROLE_Authority)
		{
			CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector::ZeroVector);
		}
	}

	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.RemoveDynamic(this, &ATPSCharacter::InitWeapon);
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATPSCharacter::DelayedInitWeapon);
	}

	InitAnimation();
	//this->OnTakeAnyDamage.AddDynamic(this, &ATPSCharacter::TakeAnyDamage);
}

void ATPSCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	if (NewInputComponent)
	{
		NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATPSCharacter::InputAxisX);
		NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATPSCharacter::InputAxisY);

		NewInputComponent->BindAction(TEXT("ChangeToSprint"), EInputEvent::IE_Pressed, this, &ATPSCharacter::InputSprintPressed);
		NewInputComponent->BindAction(TEXT("ChangeToWalk"), EInputEvent::IE_Pressed, this, &ATPSCharacter::InputWalkPressed);
		NewInputComponent->BindAction(TEXT("AimEvent"), EInputEvent::IE_Pressed, this, &ATPSCharacter::InputAimPressed);
		NewInputComponent->BindAction(TEXT("ChangeToSprint"), EInputEvent::IE_Released, this, &ATPSCharacter::InputSprintReleased);
		NewInputComponent->BindAction(TEXT("ChangeToWalk"), EInputEvent::IE_Released, this, &ATPSCharacter::InputWalkReleased);
		NewInputComponent->BindAction(TEXT("AimEvent"), EInputEvent::IE_Released, this, &ATPSCharacter::InputAimReleased);

		NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATPSCharacter::InputAttackPressed);
		NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATPSCharacter::InputAttackReleased);
		NewInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ATPSCharacter::TryReloadWeapon);

		NewInputComponent->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Released, this, &ATPSCharacter::TrySwitchNextWeapon);
		NewInputComponent->BindAction(TEXT("SwitchPreviosWeapon"), EInputEvent::IE_Released, this, &ATPSCharacter::TrySwitchPreviosWeapon);

		NewInputComponent->BindAction(TEXT("AbilityAction"), EInputEvent::IE_Released, this, &ATPSCharacter::TryAbilityEnabled);
		NewInputComponent->BindAction(TEXT("DropCurrentWeapon"), EInputEvent::IE_Released, this, &ATPSCharacter::DropCurrentWeapon);
		NewInputComponent->BindAction(TEXT("Dashing"), EInputEvent::IE_Pressed, this, &ATPSCharacter::Dash);


		TArray<FKey> HotKeys;
		HotKeys.Add(EKeys::One);
		HotKeys.Add(EKeys::Two);
		HotKeys.Add(EKeys::Three);
		HotKeys.Add(EKeys::Four);
		HotKeys.Add(EKeys::Five);
		HotKeys.Add(EKeys::Six);
		HotKeys.Add(EKeys::Seven);
		HotKeys.Add(EKeys::Eight);
		HotKeys.Add(EKeys::Nine);
		HotKeys.Add(EKeys::Zero);
		

		NewInputComponent->BindKey(HotKeys[1], IE_Pressed, this, &ATPSCharacter::TKeyPressed<1>);
		NewInputComponent->BindKey(HotKeys[2], IE_Pressed, this, &ATPSCharacter::TKeyPressed<2>);
		NewInputComponent->BindKey(HotKeys[3], IE_Pressed, this, &ATPSCharacter::TKeyPressed<3>);
		NewInputComponent->BindKey(HotKeys[4], IE_Pressed, this, &ATPSCharacter::TKeyPressed<4>);
		NewInputComponent->BindKey(HotKeys[5], IE_Pressed, this, &ATPSCharacter::TKeyPressed<5>);
		NewInputComponent->BindKey(HotKeys[6], IE_Pressed, this, &ATPSCharacter::TKeyPressed<6>);
		NewInputComponent->BindKey(HotKeys[7], IE_Pressed, this, &ATPSCharacter::TKeyPressed<7>);
		NewInputComponent->BindKey(HotKeys[8], IE_Pressed, this, &ATPSCharacter::TKeyPressed<8>);
		NewInputComponent->BindKey(HotKeys[9], IE_Pressed, this, &ATPSCharacter::TKeyPressed<9>);
		NewInputComponent->BindKey(HotKeys[0], IE_Pressed, this, &ATPSCharacter::TKeyPressed<0>);
	}
}

void ATPSCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATPSCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATPSCharacter::InputAttackPressed()
{
	if (HealthComponent && HealthComponent->GetIsAlive())
	{
		AttackCharEvent(true);
	}
}

void ATPSCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ATPSCharacter::InputSprintPressed()
{
	SprintRunEnabled = true;
	ChangeMovementState();
}

void ATPSCharacter::InputSprintReleased()
{
	SprintRunEnabled = false;
	ChangeMovementState();
}

void ATPSCharacter::InputWalkPressed()
{
	WalkEnabled = true;
	ChangeMovementState();
}

void ATPSCharacter::InputWalkReleased()
{
	WalkEnabled = false;
	ChangeMovementState();
}

void ATPSCharacter::InputAimPressed()
{
	AimEnabled = true;
	ChangeMovementState();
}

void ATPSCharacter::InputAimReleased()
{
	AimEnabled = false;
	ChangeMovementState();
}

void ATPSCharacter::MovementTick(float DeltaTime)
{
	if (HealthComponent && HealthComponent->GetIsAlive() && GetController() && GetController()->IsLocalPlayerController())
	{
		AddMovementInput(FVector::ForwardVector, AxisX);
		AddMovementInput(FVector::RightVector, AxisY);

		//FString SEnum = UEnum::GetValueAsString(GetMovementState());
		//UE_LOG(LogTPS_Net, Warning, TEXT("Movement State - %s"), *SEnum);

		if (MovementState == EMovementState::SprintRun_State)
		{
			FVector myRotationVector = FVector(AxisX, AxisY, 0.0f);
			FRotator myRotator = myRotationVector.ToOrientationRotator();

			SetActorRotation(FQuat(myRotator));
			SetActorRotationByYaw_OnServer(myRotator.Yaw);
		}
		else
		{
			APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
			if (myController)
			{
				FHitResult ResultHit;
				//myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);
				myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);

				const float FindRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;

				SetActorRotation(FQuat(FRotator(0.0f, FindRotatorResultYaw, 0.0f)));
				SetActorRotationByYaw_OnServer(FindRotatorResultYaw);

				if (CurrentWeapon) //TODO Reduce mb yes when no fire time
				{
					FVector Displacement = FVector::ZeroVector;
					bool bIsReduceDispersion = false;
					switch (MovementState)
					{
					case  EMovementState::Aim_State:
						Displacement = FVector(0.0f, 0.0f, 160.0f);
						bIsReduceDispersion = true;
						break;
					case EMovementState::AimWalk_State:
						Displacement = FVector(0.0f, 0.0f, 160.0f);
						bIsReduceDispersion = true;
						break;
					case EMovementState::Walk_State:
						Displacement = FVector(0.0f, 0.0f, 120.0f);
						break;
					case EMovementState::Run_State:
						Displacement = FVector(0.0f, 0.0f, 120.0f);
						break;
					case EMovementState::SprintRun_State:
						break;
					default:
						break;
					}
					//CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
					CurrentWeapon->UpdateWeaponByCharacterMovementState_OnServer(ResultHit.Location + Displacement, bIsReduceDispersion);
					//aim  cursor like 3d Widget?
				}
			}
		}
	}
	
}


void ATPSCharacter::AttackCharEvent(bool bIsFiring)
{
	//AWeaponDefault* myWeapon = nullptr;
	//myWeapon = GetCurrentWeapon();

	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon && !EquipAnimInProgress)
	{
		//ToDo Check melee or range
		myWeapon->SetWeaponStateFire_OnServer(bIsFiring);
	}
	else
	{
		UE_LOG(LogTPSCharacter, Warning, TEXT("ATPSCharacter::AttackCharEvent - CurrentWeapon -NULL or Equip animation in progress"));
	}
}

void ATPSCharacter::CharacterUpdate()
{
	//AWeaponDefault* myWeapon = nullptr;
	//myWeapon = GetCurrentWeapon();

	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		float ResSpeed = 600.0f;
		switch (MovementState)
		{
		case EMovementState::Aim_State:
			ResSpeed = myWeapon->WeaponSetting.MovementInfo.AimSpeedNormal;
			break;
		case EMovementState::AimWalk_State:
			ResSpeed = myWeapon->WeaponSetting.MovementInfo.AimSpeedWalk;
			break;
		case EMovementState::Walk_State:
			ResSpeed = myWeapon->WeaponSetting.MovementInfo.WalkSpeedNormal;
			break;
		case EMovementState::Run_State:
			ResSpeed = myWeapon->WeaponSetting.MovementInfo.RunSpeedNormal;
			break;
		case EMovementState::SprintRun_State:
			ResSpeed = myWeapon->WeaponSetting.MovementInfo.SprintRunSpeedRun;
			break;
		default:
			break;
		}
		SetMovementSpeed_OnServer(ResSpeed);
		//GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
	}
	else
		UE_LOG(LogTPSCharacter, Warning, TEXT("ATPSCharacter::CharacterUpdate - CurrentWeapon -NULL"));
}

void ATPSCharacter::ChangeMovementState()
{
	const FVector VelocityNormal = GetVelocity().GetSafeNormal();
	const float Direction = FVector::DotProduct(GetActorForwardVector(), VelocityNormal);

	EMovementState NewState = EMovementState::Run_State;
	if (!WalkEnabled && !SprintRunEnabled && !AimEnabled)
	{
		NewState = EMovementState::Run_State;
	}
	else
	{
		if (SprintRunEnabled && !GetVelocity().IsZero() && Direction > 0.0f)
		{
			WalkEnabled = false;
			AimEnabled = false;
			NewState = EMovementState::SprintRun_State;
		}
		else
		{
			if (WalkEnabled && !SprintRunEnabled && AimEnabled)
			{
				NewState = EMovementState::AimWalk_State;
			}
			else
			{
				if (WalkEnabled && !SprintRunEnabled && !AimEnabled)
				{
					NewState = EMovementState::Walk_State;
				}
				else
				{
					if (!WalkEnabled && !SprintRunEnabled && AimEnabled)
					{
						NewState = EMovementState::Aim_State;
					}
				}
			}
		}	
	}

	SetMovementState_OnServer(NewState);
	//CharacterUpdate();

	//Weapon state update
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon_OnServer(NewState);
	}
}

AWeaponDefault* ATPSCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

bool ATPSCharacter::GetIsAlive() const
{
	/*bool result = false;
	if (HealthComponent)
	{
		result = HealthComponent->GetIsAlive();
	}
	return result;*/

	return HealthComponent && HealthComponent->GetIsAlive() ? true : false;
}

void ATPSCharacter::DelayedInitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)
{
	FTimerDelegate TimerDelegate;
	TimerDelegate.BindUObject(this, &ATPSCharacter::InitWeapon, IdWeaponName, WeaponAdditionalInfo, NewCurrentIndexWeapon);
	UE_LOG(LogTPSCharacter, Warning, TEXT("Delay"));
	GetWorldTimerManager().SetTimer(TimerHandle_SwitchWeapon, TimerDelegate, 1.0f, false);
}


void ATPSCharacter::InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)
{
	//On Server

	if (CurrentWeapon)
	{
		if (CurrentWeapon->myCurrentLazer)
		{
			CurrentWeapon->DestroyLazer_Multicast(CurrentWeapon->myCurrentLazer);
		}
		RemoveCurrentWeapon();
	}

	UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI && myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
	{
		if (myWeaponInfo.WeaponClass)
		{
			FVector SpawnLocation = FVector::ZeroVector;
			FRotator SpawnRotation = FRotator::ZeroRotator;

			FActorSpawnParameters SpawnParams;
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			SpawnParams.Owner = this;
			SpawnParams.Instigator = GetInstigator();

			AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
			if (myWeapon)
			{
				const FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
				myWeapon->AttachToComponent(GetMesh(), Rule, WeaponSocketName);
				CurrentWeapon = myWeapon;

				myWeapon->IdWeaponName = IdWeaponName;
				myWeapon->WeaponSetting = myWeaponInfo;
				//myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
				myWeapon->UpdateStateWeapon_OnServer(MovementState);
				myWeapon->AdditionalWeaponInfo = WeaponAdditionalInfo;
				CurrentIndexWeapon = NewCurrentIndexWeapon;

				myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATPSCharacter::WeaponReloadStart);
				myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATPSCharacter::WeaponReloadEnd);
				myWeapon->OnWeaponFireStart.AddDynamic(this, &ATPSCharacter::WeaponFireStart);

				//after switch try reload weapon if needed
				if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CheckCanWeaponReload())
					CurrentWeapon->InitReload();

				if (InventoryComponent)
					InventoryComponent->OnWeaponAmmoAviable.Broadcast(myWeapon->WeaponSetting.WeaponType);
			}
		}
	}
	else
	{
		UE_LOG(LogTPSCharacter, Warning, TEXT("ATPSCharacter::InitWeapon - Weapon not found in table -NULL"));
	}
	
}


void ATPSCharacter::RemoveCurrentWeapon()
{
	CurrentWeapon->Destroy();
	CurrentWeapon = nullptr;
}

void ATPSCharacter::TryReloadWeapon()
{
	if (HealthComponent && HealthComponent->GetIsAlive()  && CurrentWeapon && !CurrentWeapon->WeaponReloading) 
	{
		TryReloadWeapon_OnServer();
	}
}

void ATPSCharacter::Dash() //ToDo refactoring to switch
{
	if (HealthComponent && HealthComponent->GetIsAlive() && NumberOfDash > 0 && !bIsDashing && CounterDash > 0)
	{
		Dash_OnServer();
	}
}

void ATPSCharacter::Dash_OnServer_Implementation()
{
    if (AxisX == 0 && AxisY == 0) 
		return;

    // �������� ������ ��������, ����������� ��� � �������� �� ��������� DashDistance
    FVector DashingVector = FVector(AxisX, AxisY, 0).GetSafeNormal() * DashDistance;

    bIsDashing = true;
    SetActorLocation_OnServer(DashingVector);
    CounterDash--;
    GetWorldTimerManager().SetTimer(TimerHandle_ReloadingDashTimer, this, &ATPSCharacter::ReloadingDash, TimeForDashReload, true);
    bIsDashing = false;
    Dash_BP();
}
		

void ATPSCharacter::Dash_BP_Implementation()
{
	// in BP
}

void ATPSCharacter::ReloadingDash()
{
	if (NumberOfDash > CounterDash)
	{
		CounterDash++;
	}
	else
	{
		GetWorldTimerManager().ClearTimer(TimerHandle_ReloadingDashTimer);
	}
}

void ATPSCharacter::DropCurrentWeapon()
{
	if (InventoryComponent)
	{
		InventoryComponent->DropWeaponByIndex_OnServer(CurrentIndexWeapon);
	}
}

void ATPSCharacter::TrySwitchWeaponToIndexByKeyInput_OnServer_Implementation(int32 ToIndex)
{
	bool bIsSucces = false;
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.IsValidIndex(ToIndex) && CanEquip())
	{
		if (CurrentIndexWeapon != ToIndex && InventoryComponent)
		{
			int32 OldIndex = CurrentIndexWeapon;
			FAdditionalWeaponInfo OldInfo;

			if (CurrentWeapon)
			{
				OldInfo = CurrentWeapon->AdditionalWeaponInfo;
				//if (CurrentWeapon->WeaponReloading) //StopReload when switch weapon 
					//CurrentWeapon->CancelReload();
			}

			if (EquipAnim)
			{
				PlayAnim_MultiCast(EquipAnim);
				EquipAnimInProgress = true;
			}

			bIsSucces = InventoryComponent->SwitchWeaponByIndex(ToIndex, OldIndex, OldInfo);
		}
	}
}

void ATPSCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

int32 ATPSCharacter::GetCurrentWeaponIndex()
{
	return CurrentIndexWeapon;
}

TArray<UTPS_StateEffect*> ATPSCharacter::GerCurrentEffectsOnChar()
{
	return Effects;
}

void ATPSCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.WeaponType, AmmoTake);
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	}

	WeaponReloadEnd_BP(bIsSuccess);
}


void ATPSCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

void ATPSCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
	// in BP
}

void ATPSCharacter::WeaponFireStart(UAnimMontage* Anim)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	}

	WeaponFireStart_BP(Anim);
}

void ATPSCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
	//in BP
}

UDecalComponent* ATPSCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

void ATPSCharacter::TrySwitchNextWeapon() //ToDo switch to pickup weapon
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1 && CanEquip())
	{
		//we have more then one weapon go switch
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			//if (CurrentWeapon->WeaponReloading)
				//CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			if (EquipAnim)
			{
				PlayAnim_MultiCast(EquipAnim);
				EquipAnimInProgress = true;
			}

			//PlayAnim_OnServer(EquipAnim);
			if (InventoryComponent->SwitchWeaponToIndexByNextPreviosIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo, true))
			{
				
			}
		}
	}
}


void ATPSCharacter::TrySwitchPreviosWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1 && CanEquip())
	{
		//We have more then one weapon go switch
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			//if (CurrentWeapon->WeaponReloading)
				//CurrentWeapon->CancelReload();
		}

		if (EquipAnim)
		{
			PlayAnim_MultiCast(EquipAnim);
			EquipAnimInProgress = true;
		}

		if (InventoryComponent)
		{
			//PlayAnim_OnServer(EquipAnim);
			//InventoryComponent->SetAdditionalInfoWeapon(OldIndex, GetCurrentWeapon()->AdditiolanWeaponInfo);
			if (InventoryComponent->SwitchWeaponToIndexByNextPreviosIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo, false))
			{
				
			}
		}
	}
}

void ATPSCharacter::TryAbilityEnabled()
{
	if (HealthComponent && HealthComponent->GetIsAlive() && AbilityEffect && bIsCanUseAbility)
	{
		AbilityTimer = AbilityTime.AbilityTimer;

		UTPS_StateEffect* NewEffect = NewObject<UTPS_StateEffect>(this, AbilityEffect);
		if (NewEffect)
		{
			NewEffect->InitObject(this, FName("Spine_01"));
		}
	}
}


EPhysicalSurface ATPSCharacter::GetSurfaceType() 
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;

	if (HealthComponent && HealthComponent->GetCurrentShield() <= 0.0f && GetMesh()) 
	{
		UMaterialInterface* myMaterial = GetMesh()->GetMaterial(0);
		if (myMaterial)
		{
			Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
		}
	}

	return Result;
}

EMovementState ATPSCharacter::GetMovementState() const
{
	return MovementState;
}

TArray<UTPS_StateEffect*> ATPSCharacter::GetAllCurrentStateEffects()
{
	return Effects;
}

void ATPSCharacter::RemoveEffect_Implementation(UTPS_StateEffect* RemovedEffect)
{
	Effects.Remove(RemovedEffect);

	if (!RemovedEffect->bIsAutoDestroyParticleEffect)
	{
		SwitchEffect(RemovedEffect, false);
		EffectRemove = RemovedEffect;
	}
}

void ATPSCharacter::AddEffect_Implementation(UTPS_StateEffect* NewEffect)
{
	Effects.Add(NewEffect);

	if (!NewEffect->bIsAutoDestroyParticleEffect)
	{
		SwitchEffect(NewEffect, true);
		EffectAdd = NewEffect;
	}
	else
	{
		if (NewEffect->ParticleEffect)
		{
			ExecuteEffectAdded_OnServer(NewEffect->ParticleEffect);
		}
	}
}


void ATPSCharacter::TryReloadWeapon_OnServer_Implementation()
{
	if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound && CurrentWeapon->CheckCanWeaponReload())
		CurrentWeapon->InitReload();
}

void ATPSCharacter::SetActorRotationByYaw_OnServer_Implementation(float Yaw)
{
	SetActorRotationByYaw_Multicast(Yaw);
}

void ATPSCharacter::SetActorRotationByYaw_Multicast_Implementation(float Yaw)
{
	if (Controller && !Controller->IsLocalPlayerController())
	{
		SetActorRotation(FQuat(FRotator(0.0f, Yaw, 0.0f)));
	}
}

void ATPSCharacter::SetActorLocation_OnServer_Implementation(FVector NewLocation)
{
	if (HasAuthority())
	{
		FVector FinalLocation = FVector(0);
		FinalLocation = GetActorLocation() + NewLocation;
		SetActorLocation(FinalLocation, true);
	}
}

void ATPSCharacter::SetMovementState_OnServer_Implementation(EMovementState NewState)
{
	SetMovementState_Multicast(NewState);
	CharacterUpdate(); //use here because movement data init on server!
}

void ATPSCharacter::SetMovementState_Multicast_Implementation(EMovementState NewState)
{
	MovementState = NewState;
	//CharacterUpdate();
}

void ATPSCharacter::CharDead_BP_Implementation()
{
	//BP
}

void ATPSCharacter::CharDead()
{
	CharDead_BP();

	if (HasAuthority())
	{
		float TimeAnim = 0.0f;
		const int32 rnd = FMath::RandHelper(DeadsAnim.Num());
		if (DeadsAnim.IsValidIndex(rnd) && DeadsAnim[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
		{
			TimeAnim = DeadsAnim[rnd]->GetPlayLength();
			PlayAnim_MultiCast(DeadsAnim[rnd]);
			//GetMesh()->GetAnimInstance()->Montage_Play(DeadsAnim[rnd]);
		}

		if (GetController())
		{
			GetController()->UnPossess();
		}

		GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer, this, &ATPSCharacter::EnableRagdoll_MultiCast, TimeAnim - 0.01f, false); //TODO hard code impuls

		if (GetCurrentWeapon() && GetCurrentWeapon()->SkeletalMeshWeapon) //TODO on multicast
		{
			const FDetachmentTransformRules Rules(EDetachmentRule::KeepWorld, false);
			GetCurrentWeapon()->DetachFromActor(Rules);
			GetCurrentWeapon()->SetLifeSpan(LifeSpanOnDeath);
			GetCurrentWeapon()->GetMesh()->SetCollisionObjectType(ECC_PhysicsBody);
			GetCurrentWeapon()->GetMesh()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Block);
			GetCurrentWeapon()->GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
			GetCurrentWeapon()->GetMesh()->SetSimulatePhysics(true);
		}

		SetLifeSpan(LifeSpanOnDeath);
	}	
	else
	{
		if (GetCursorToWorld())
		{
			GetCursorToWorld()->SetVisibility(false);
		}

		AttackCharEvent(false);
	}

	if (GetCapsuleComponent())
	{
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
	}
}

void ATPSCharacter::EnableRagdoll_MultiCast_Implementation()
{
	if (GetMesh())
	{
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Block);
		GetMesh()->SetCollisionObjectType(ECC_PhysicsBody);
		GetMesh()->SetSimulatePhysics(true);
	}
}

float ATPSCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	if (HealthComponent && HealthComponent->GetIsAlive())
	{
		HealthComponent->ChangeHealthValue_OnServer(-DamageAmount);
	}

	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);
		if (myProjectile)
		{
			UTypes::AddEffectBySurfaceType(this, FName("Spine_01"), myProjectile->ProjectileSetting.Effect, GetSurfaceType()); 
		}
	}

	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();

	if (myWeapon && myWeapon->WeaponReloading)
		myWeapon->CancelReload();

	return ActualDamage;
}

void ATPSCharacter::InitAnimation()
{
	auto EquipStartNotify = AnimUtils::FindNotifyByClass<UTPSEquipStartAnimNotify>(EquipAnim); //Really End not Start
	if (EquipStartNotify)
	{
		EquipStartNotify->OnNotified.AddUObject(this, &ATPSCharacter::OnEquipStart);
	}
	else
	{
		UE_LOG(LogTPSCharacter, Error, TEXT("No set end switch notify"));
		checkNoEntry();
	}
}

void ATPSCharacter::OnEquipStart(USkeletalMeshComponent* MeshComp)
{
	if (GetMesh() == MeshComp)
	{
		EquipAnimInProgress = false;
	}
}

bool ATPSCharacter::CanEquip() const
{
	return !EquipAnimInProgress;
}

void ATPSCharacter::AbilityTick(float DeltaTime)
{
	if (AbilityTimer < 0.f)
	{
		bIsCanUseAbility = true;
	}
	else
	{
		bIsCanUseAbility = false;
		AbilityTimer -= DeltaTime;
	}	
}

void ATPSCharacter::PlayAnim_MultiCast_Implementation(UAnimMontage* Anim)
{
	/*if (GetMesh() && GetMesh()->GetAnimInstance())
	{
		GetMesh()->GetAnimInstance()->Montage_Play(Anim);
	}*/

	PlayAnimMontage(Anim);
}

void ATPSCharacter::SetMovementSpeed_OnServer_Implementation(float Speed)
{
	SetMovementSpeed_Multicast(Speed);
}

void ATPSCharacter::SetMovementSpeed_Multicast_Implementation(float Speed)
{
	GetCharacterMovement()->MaxWalkSpeed = Speed;
}

void ATPSCharacter::PlayAnim_OnServer_Implementation(UAnimMontage* Anim)
{
	PlayAnim_MultiCast_Implementation(Anim);;
}

void ATPSCharacter::EffectAdd_OnRep()
{
	if (EffectAdd)
	{
		SwitchEffect(EffectAdd, true);
	}
}

void ATPSCharacter::EffectRemove_OnRep()
{
	if (EffectRemove)
	{
		SwitchEffect(EffectRemove, false);
	}
}

void ATPSCharacter::SwitchEffect(UTPS_StateEffect* Effect, bool bIsAdd)
{
	if (bIsAdd)
	{
		if (Effect && Effect->ParticleEffect)
		{
			FName NameBoneToAttached = Effect->NameBone;
			FVector Loc = FVector(0);

			USkeletalMeshComponent* myMesh = GetMesh();
			if (myMesh)
			{
				UParticleSystemComponent* newParticleSystem = UGameplayStatics::SpawnEmitterAttached(Effect->ParticleEffect, myMesh, NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
				ParticleSystemEffects.Add(newParticleSystem);
			}
		}
	}
	else
	{
		if (Effect && Effect->ParticleEffect && ParticleSystemEffects.Num() > 0)
		{
			int32 i = 0;
			bool bIsFind = false;
			while (i < ParticleSystemEffects.Num() && !bIsFind)
			{
				if (ParticleSystemEffects[i]->Template && Effect->ParticleEffect && Effect->ParticleEffect == ParticleSystemEffects[i]->Template)
				{
					bIsFind = true;
					ParticleSystemEffects[i]->DeactivateSystem();
					ParticleSystemEffects[i]->DestroyComponent();
					ParticleSystemEffects.RemoveAt(i);
				}
				i++;
			}
		}
	}
}

void ATPSCharacter::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ExecuteFX)
{
	ExecuteEffectAdded_MultiCast(ExecuteFX);
}

void ATPSCharacter::ExecuteEffectAdded_MultiCast_Implementation(UParticleSystem* ExecuteFX)
{
	UTypes::ExecuteEffectAdded(ExecuteFX, this, FVector(0), FName("Spine_01"));
}

bool ATPSCharacter::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (int32 i = 0; i < Effects.Num(); i++)
	{
		if (Effects[i])
		{
			Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags);	// (this makes those subobjects 'supported', and from here on those objects may have reference replicated)		
		}
	}
	return Wrote;
}



void ATPSCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATPSCharacter, MovementState);
	DOREPLIFETIME(ATPSCharacter, CurrentWeapon);
	DOREPLIFETIME(ATPSCharacter, CurrentIndexWeapon);
	DOREPLIFETIME(ATPSCharacter, EffectRemove);
	DOREPLIFETIME(ATPSCharacter, EffectAdd);
	DOREPLIFETIME(ATPSCharacter, Effects);
}