// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS/Character/TPSHealthComponent.h"
#include "TPSHealthComponent.h"
#include "Net/UnrealNetwork.h"

UTPSHealthComponent::UTPSHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	SetIsReplicatedByDefault(true);
}

void UTPSHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	check(MaxHealth > 0.0f);
	SetCurrentHealth(MaxHealth);
}

void UTPSHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

float UTPSHealthComponent::GetCurrentHealth() const
{
	return Health;
}

void UTPSHealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = FMath::Clamp(NewHealth, 0.0f, MaxHealth);
	OnHealthChange.Broadcast(Health, NewHealth - Health);
}

bool UTPSHealthComponent::GetIsAlive() const
{
	return bIsAlive;
}

void UTPSHealthComponent::ChangeHealthValue_OnServer_Implementation(float ChangeValue)
{
	if (bIsAlive)
	{
		ChangeValue = ChangeValue * CoefDamage;
		Health = FMath::Clamp(Health + ChangeValue, 0.0f, MaxHealth);

		HealthChangeEvent_MultiCast(Health, ChangeValue);
		//OnHealthChange.Broadcast(Health, ChangeValue);

		if (Health <= 0.0f)
		{
			bIsAlive = false;
			DeadEvent_MultiCast();
			//OnDead.Broadcast();
		}
	}
}

void UTPSHealthComponent::DeadEvent_MultiCast_Implementation()
{
	OnDead.Broadcast();
}

void UTPSHealthComponent::HealthChangeEvent_MultiCast_Implementation(float newHealth, float Damage)
{
	OnHealthChange.Broadcast(newHealth, Damage);
}


void  UTPSHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UTPSHealthComponent, Health);
	DOREPLIFETIME(UTPSHealthComponent, bIsAlive);
}