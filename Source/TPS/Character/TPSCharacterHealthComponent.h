// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TPS/Character/TPSHealthComponent.h"

#include "TPSCharacterHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);

UCLASS()
class TPS_API UTPSCharacterHealthComponent : public UTPSHealthComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Shield")
	FOnShieldChange OnShieldChange;

	FTimerHandle TimerHandle_CoolDownShieldTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	float CoolDownShieldRecoveryTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	float ShieldRecoveryValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	float ShieldRecoveryRate = 0.1f;

protected:
	UPROPERTY(Replicated)
	float Shield = 100.0f;
	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadWrite, Category = "Shield", meta = (ClampMin = "0.0", ClampMax = "1000.0"))
	float MaxShield = 100.0f;


	virtual void BeginPlay() override;

public:

	void ChangeHealthValue_OnServer(float ChangeValue) override;

	UFUNCTION(BlueprintCallable, Category = "Shield")
	float GetCurrentShield() const;
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Shield")
	void ChangeShieldValue_OnServer(float ChangeValue);
	void RecoveryShield();
	UFUNCTION(BlueprintCallable, Category = "Shield")
	void SetCurrentShield(float NewShield);
	UFUNCTION(BlueprintCallable)
	float GetShieldValue();
	UFUNCTION(NetMulticast, Reliable)
	void ShieldChangeEvent_MultiCast(float newShield, float Damage);
};

