// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameFramework/Actor.h"
#include "TPS/FuncLibrary/Types.h"
#include "TPS/Interface/TPS_IGameActor.h"
#include "TPSCharacter.generated.h"

class UTPSCharacterHealthComponent;
class UTPSInventoryComponent;
class AWeaponDefault;
class UTPS_StateEffect;

UCLASS(Blueprintable)
class ATPSCharacter : public ACharacter, public ITPS_IGameActor
{
	GENERATED_BODY()

public:
	ATPSCharacter();

	FTimerHandle TimerHandle_RagDollTimer;
	FTimerHandle TimerHandle_ReloadingDashTimer;
	FTimerHandle TimerHandle_SwitchWeapon;

	float AbilityTimer = 0.0f;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	
protected:
	virtual void BeginPlay() override;

	bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

	void InputAxisY(float Value);
	void InputAxisX(float Value);

	void InputAttackPressed();
	void InputAttackReleased();

	void InputSprintPressed();
	void InputSprintReleased();

	void InputWalkPressed();
	void InputWalkReleased();

	void InputAimPressed();
	void InputAimReleased();


	//Inventory Inputs
	void TrySwitchNextWeapon();
	void TrySwitchPreviosWeapon();
	//Ability Inputs
	void TryAbilityEnabled();

	template<int32 Id>
	void TKeyPressed()
	{
		TrySwitchWeaponToIndexByKeyInput_OnServer(Id);
	}
	//Inputs End

	//Input flags
	float AxisX = 0.0f;
	float AxisY = 0.0f;

	bool SprintRunEnabled = false;
	bool WalkEnabled = false;
	bool AimEnabled = false;
	
	bool bIsCanUseAbility = true;

	UPROPERTY(EditDefaultsOnly, Category = "Death")
	float LifeSpanOnDeath = 20.0f;

	UPROPERTY(Replicated)
	AWeaponDefault* CurrentWeapon = nullptr;
	UPROPERTY()
	UDecalComponent* CurrentCursor = nullptr;
	
	UPROPERTY(Replicated)
	TArray<UTPS_StateEffect*> Effects;
	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
	UTPS_StateEffect* EffectAdd = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
	UTPS_StateEffect* EffectRemove = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	TArray<UParticleSystemComponent*> ParticleSystemEffects;

	UPROPERTY(Replicated)
	int32 CurrentIndexWeapon = 0;

	UPROPERTY()
	FName WeaponSocketName = "WeaponSocketRightHand";

	UFUNCTION()
	void CharDead();
	UFUNCTION(NetMulticast, Reliable)
	void EnableRagdoll_MultiCast();

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;


private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	void InitAnimation();
	void OnEquipStart(USkeletalMeshComponent* MeshComp);

	bool EquipAnimInProgress = false;
	bool CanEquip() const;

public:
	//Cursor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement") // movement init from table with weapon 
	///FCharacterSpeed MovementInfo;
	UPROPERTY(Replicated)
	EMovementState MovementState = EMovementState::Run_State;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
	TArray<UAnimMontage*> DeadsAnim;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
	TArray<UAnimMontage*> HitsAnim;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
	UAnimMontage* EquipAnim;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
	TSubclassOf<UTPS_StateEffect> AbilityEffect;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability") //ToDo table with abillity 
	FAbilityInfo AbilityTime;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Dash")
	float NumberOfDash = 3;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Dash")
	float CounterDash = NumberOfDash;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "Dash")
	bool bIsDashing = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dash")
	float DashDistance = 300.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dash")
	float TimeForDashReload = 3.0f;

	//Inventory
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UTPSInventoryComponent* InventoryComponent;

	//HealthComponent
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UTPSCharacterHealthComponent* HealthComponent;


	//Tick Func
	UFUNCTION()
	void MovementTick(float DeltaTime);
	UFUNCTION()
	void AbilityTick(float DeltaTime);


	//Func
	
	void AttackCharEvent(bool bIsFiring);

	void CharacterUpdate();
	void ChangeMovementState();

	UFUNCTION(Server, Reliable)
	void TrySwitchWeaponToIndexByKeyInput_OnServer(int32 ToIndex);
	UFUNCTION()
	void DropCurrentWeapon();

	UFUNCTION()
	void DelayedInitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);
	UFUNCTION()
	void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);
	UFUNCTION(BlueprintCallable)
	void RemoveCurrentWeapon();
	UFUNCTION()
	void TryReloadWeapon();

	UFUNCTION()
	void Dash();
	UFUNCTION(Server, Reliable)
	void Dash_OnServer();
	UFUNCTION(BlueprintNativeEvent)
	void Dash_BP();
	UFUNCTION()
	void ReloadingDash();

	UFUNCTION()
	void WeaponReloadStart(UAnimMontage* Anim);
	UFUNCTION()
	void WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe);
	UFUNCTION()
	void WeaponFireStart(UAnimMontage* Anim);


	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadEnd_BP(bool bIsSuccess);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponFireStart_BP(UAnimMontage* Anim);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	int32 GetCurrentWeaponIndex();
	UFUNCTION(BlueprintCallable, BlueprintPure)
	EMovementState GetMovementState() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	TArray<UTPS_StateEffect*> GerCurrentEffectsOnChar();
	UFUNCTION(BlueprintCallable, BlueprintPure)
	UDecalComponent* GetCursorToWorld();
	UFUNCTION(BlueprintCallable, BlueprintPure)
	AWeaponDefault* GetCurrentWeapon();

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool GetIsAlive() const;

	//Interface
	EPhysicalSurface GetSurfaceType() override;
	TArray<UTPS_StateEffect*> GetAllCurrentStateEffects() override;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void RemoveEffect(UTPS_StateEffect* RemovedEffect);
	void RemoveEffect_Implementation(UTPS_StateEffect* RemovedEffect) override;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void AddEffect(UTPS_StateEffect* NewEffect);
	void AddEffect_Implementation(UTPS_StateEffect* NewEffect) override;
	//End 

	UFUNCTION(BlueprintNativeEvent)
	void CharDead_BP();

	UFUNCTION(Server, Unreliable)
	void SetActorRotationByYaw_OnServer(float Yaw);
	UFUNCTION(NetMulticast, Unreliable)
	void SetActorRotationByYaw_Multicast(float Yaw);
	UFUNCTION(Server, Reliable)
	void SetActorLocation_OnServer(FVector NewLocation);

	UFUNCTION(Server, Reliable)
	void SetMovementState_OnServer(EMovementState NewState);
	UFUNCTION(NetMulticast, Reliable)
	void SetMovementState_Multicast(EMovementState NewState);

	UFUNCTION(Server, Reliable)
	void SetMovementSpeed_OnServer(float Speed);
	UFUNCTION(NetMulticast, Reliable)
	void SetMovementSpeed_Multicast(float Speed);
	
	UFUNCTION(Server, Reliable)
	void TryReloadWeapon_OnServer();

	UFUNCTION(Server, Reliable)
	void PlayAnim_OnServer(UAnimMontage* Anim);
	UFUNCTION(NetMulticast, Reliable)
	void PlayAnim_MultiCast(UAnimMontage* Anim);

	UFUNCTION()
	void EffectAdd_OnRep();
	UFUNCTION()
	void EffectRemove_OnRep();

	UFUNCTION(Server, Reliable)
	void ExecuteEffectAdded_OnServer(UParticleSystem* ExecuteFX);
	UFUNCTION(NetMulticast, Reliable)
	void ExecuteEffectAdded_MultiCast(UParticleSystem* ExecuteFX);

	UFUNCTION()
	void SwitchEffect(UTPS_StateEffect* Effect, bool bIsAdd);

};



