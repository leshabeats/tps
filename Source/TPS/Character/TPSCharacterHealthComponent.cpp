// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS/Character/TPSCharacterHealthComponent.h"
#include "TPSCharacterHealthComponent.h"
#include "Net/UnrealNetwork.h"

DEFINE_LOG_CATEGORY_STATIC(LogTPSCharacterHealthComponent, All, All);

void UTPSCharacterHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	check(MaxShield > 0.0f);
	SetCurrentShield(MaxShield);
}

void UTPSCharacterHealthComponent::ChangeHealthValue_OnServer(float ChangeValue)
{
	const float CurrentDamage = ChangeValue * CoefDamage;

	if (Shield > 0.0f && CurrentDamage < 0.0f) //FMath::IsNearlyZero(Shield)
	{
		ChangeShieldValue_OnServer(CurrentDamage);

		if (Shield <= 0.0f)
		{
			UE_LOG(LogTPSCharacterHealthComponent, Warning, TEXT("Shield = %f"), Shield);
			//FX destroy Shield
		}
	}
	else
	{
		Super::ChangeHealthValue_OnServer(ChangeValue);
	}
}

float UTPSCharacterHealthComponent::GetCurrentShield() const
{
	return Shield;
}

void UTPSCharacterHealthComponent::ChangeShieldValue_OnServer_Implementation(float ChangeValue)
{
	Shield = FMath::Clamp(Shield + ChangeValue, 0.0f, MaxShield); 

	ShieldChangeEvent_MultiCast(Shield, ChangeValue);
	//OnShieldChange.Broadcast(Shield, ChangeValue);

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_CoolDownShieldTimer);
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CoolDownShieldTimer, this, &UTPSCharacterHealthComponent::RecoveryShield, ShieldRecoveryRate, true, CoolDownShieldRecoveryTime);
	}
}


void UTPSCharacterHealthComponent::RecoveryShield()
{
	Shield = FMath::Clamp(Shield + ShieldRecoveryValue, 0.0f, MaxShield);

	if (FMath::IsNearlyEqual(Shield, MaxShield) && GetWorld())
	{
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_CoolDownShieldTimer);
	}
	
	ShieldChangeEvent_MultiCast(Shield, ShieldRecoveryValue);
	//OnShieldChange.Broadcast(Shield, ShieldRecoveryValue);
}

void UTPSCharacterHealthComponent::SetCurrentShield(float NewShield)
{
	Shield = FMath::Clamp(NewShield, 0.0f, MaxShield);
	OnShieldChange.Broadcast(Shield, NewShield - Shield);
}

float UTPSCharacterHealthComponent::GetShieldValue()
{
	return Shield;
}

void UTPSCharacterHealthComponent::ShieldChangeEvent_MultiCast_Implementation(float newShield, float Damage)
{
	OnShieldChange.Broadcast(newShield, Damage);
}

void UTPSCharacterHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UTPSCharacterHealthComponent, Shield);
}