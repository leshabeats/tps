// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TPSHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthChange, float, Health, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDead);

USTRUCT(BlueprintType)
struct FStatsParam
{
	GENERATED_BODY()
};


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_API UTPSHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	
	UTPSHealthComponent();

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
	FOnHealthChange OnHealthChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
	FOnDead OnDead;

protected:
	
	virtual void BeginPlay() override;

	UPROPERTY(Replicated)
	float Health = 100.0f;
	UPROPERTY(Replicated)
	bool bIsAlive = true;
	UPROPERTY(Replicated,EditDefaultsOnly, BlueprintReadWrite, Category = "Health", meta = (ClampMin = "0.0", ClampMax = "1000.0"))
	float MaxHealth = 100.0f;

public:	
	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Heatlh")
	float CoefDamage = 1.0f;

	UFUNCTION(BlueprintCallable, Category = "Heatlh")
	float GetCurrentHealth() const;
	UFUNCTION(BlueprintCallable, Category = "Heatlh")
	float GetHealthPercent() const { return Health / MaxHealth; }
	UFUNCTION(BlueprintCallable, Category = "Heatlh")
	void SetCurrentHealth(float NewHealth);
	UFUNCTION(BlueprintCallable, Category = "Heatlh")
	bool GetIsAlive() const;
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Heatlh")
	virtual void ChangeHealthValue_OnServer(float ChangeValue);
	
	UFUNCTION(NetMulticast, Reliable)
	void HealthChangeEvent_MultiCast(float newHealth, float Damage);
	UFUNCTION(NetMulticast, Reliable)
	void DeadEvent_MultiCast();
};
