// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault_Grenade.h"
#include "Kismet/GameplayStatics.h"
#include "TPS/Interface/TPS_IGameActor.h"
#include "Perception/AISense_Damage.h"
#include "DrawDebugHelpers.h"


int32 DebugExplodeShow = 1;
FAutoConsoleVariableRef CVARExplodeShow(
	TEXT("TPS.DebugExplode"),
	DebugExplodeShow,
	TEXT("Draw Debug for Explode"),
	ECVF_Cheat);


void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();

}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplose(DeltaTime);
}

void AProjectileDefault_Grenade::TimerExplose(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplose > TimeToExplose)
		{
			Explose();
		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (!TimerEnabled)
	{
		Explose();
	}

	HitResult = Hit;

	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
	//Init Grenade
	TimerEnabled = true;
}

void AProjectileDefault_Grenade::Explose()
{
	if (true)//DebugExplodeShow)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMinRadiusDamage, 12, FColor::Green, false, 12.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMaxRadiusDamage, 12, FColor::Red, false, 12.0f);
	}

	TimerEnabled = false;
	
	SpawnFX_MultiCast(ProjectileSetting.ExploseFX);
	SpawnSound_MultiCast(ProjectileSetting.ExploseSound);

	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSetting.ExploseMaxDamage,
		ProjectileSetting.ExploseMaxDamage * 0.2f,
		GetActorLocation(),
		ProjectileSetting.ProjectileMinRadiusDamage,
		ProjectileSetting.ProjectileMaxRadiusDamage,
		5,
		NULL, IgnoredActor, this, GetController());

	UAISense_Damage::ReportDamageEvent(GetWorld(), HitResult.GetActor(), GetInstigator(), ProjectileSetting.ProjectileDamage, HitResult.Location, HitResult.Location);

	this->Destroy();
}

AController* AProjectileDefault_Grenade::GetController() const
{
	const auto Pawn = Cast<APawn>(GetOwner());
	return Pawn ? Pawn->GetController() : nullptr;
}

void AProjectileDefault_Grenade::SpawnFX_MultiCast_Implementation(UParticleSystem* Fx)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), Fx, GetActorLocation(), GetActorRotation(), FVector(1.0f));
}

void AProjectileDefault_Grenade::SpawnSound_MultiCast_Implementation(USoundBase* Sound)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), Sound, GetActorLocation());
}

