// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TPS/FuncLibrary/Types.h"
#include "TPS/Weapons/Projectiles/ProjectileDefault.h"
#include "TPS/Interface/TPS_IGameActor.h"
#include "TPS/TPS_Lazer.h"
#include "WeaponDefault.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFireStart, UAnimMontage*, AnimFireChar);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart, UAnimMontage*, AnimReloadChar);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, bool, bIsSuccess, int32, AmmoSafe);

class USkeletalMeshComponent;
class UStaticMeshComponent;
class UArrowComponent;

UCLASS()
class TPS_API AWeaponDefault : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AWeaponDefault();

	FOnWeaponFireStart OnWeaponFireStart;
	FOnWeaponReloadStart OnWeaponReloadStart;
	FOnWeaponReloadEnd OnWeaponReloadEnd;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UArrowComponent* ShootLocation = nullptr;

	UPROPERTY()
	FWeaponInfo WeaponSetting;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
	FAdditionalWeaponInfo AdditionalWeaponInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Lazer")
	ATPS_Lazer* myCurrentLazer = nullptr;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Lazer")
	bool bIsAvailableLazer = false;

	FName IdWeaponName;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void FireTick(float DeltaTime);
	void ReloadTick(float DeltaTime);
	void DispersionTick(float DeltaTime);
	void ClipDropTick(float DeltaTime);
	void ShellDropTick(float DeltaTime);
	void WeaponInit();

	void Fire();

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Dispersion")
	float CoefLowHealthDispersion = 2.0f;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void SetWeaponStateFire_OnServer(bool bIsFire);
	bool CheckWeaponCanFire();

	FProjectileInfo GetProjectile();

	UFUNCTION(Server, Reliable)
	void UpdateStateWeapon_OnServer(EMovementState NewMovementState);
	void ChangeDispersionByShot();
	float GetCurrentDispersion() const;
	FVector ApplyDispersionToShoot(FVector DirectionShoot) const;

	FVector GetFireEndLocation() const;
	int8 GetNumberProjectileByShot() const;

	//Timers
	float FireTimer = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
	float ReloadTimer = 0.0f;

	//flags
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	bool WeaponFiring = false;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
	bool WeaponReloading = false;
	bool WeaponAiming = false;

	bool BlockFire = false;



	//Dispersion
	UPROPERTY(Replicated)
	bool ShouldReduceDispersion = false;
	float CurrentDispersion = 0.0f;
	float CurrentDispersionMax = 1.0f;
	float CurrentDispersionMin = 0.1f;
	float CurrentDispersionRecoil = 0.1f;
	float CurrentDispersionReduction = 0.1f;

	//Timer Drop Magazine on reload
	bool DropClipFlag = false;
	float DropClipTimer = -1.0f;

	//Timer Drop Bullets on fire
	bool DropShellFlag = false;
	float DropShellTimer = -1.0f;
	
	UPROPERTY(Replicated)
	FVector ShootEndLocation = FVector(0);


	UFUNCTION()
	USkeletalMeshComponent* GetMesh() { return SkeletalMeshWeapon; }

	UFUNCTION(BlueprintCallable)
	int32 GetWeaponRound();
	UFUNCTION()
	void InitReload();
	void FinishReload();
	void CancelReload();

	bool CheckCanWeaponReload();
	int8 GetAviableAmmoForReload();
	
	UFUNCTION(Server, Reliable)
	void InitDropMesh_OnServer(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass);
	UFUNCTION(NetMulticast, Unreliable)
	void InitDropMesh_MultiCast(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass, FVector LocalDir);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool ShowDebug = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	float SizeVectorToChangeShootDirectionLogic = 100.0f;

	UFUNCTION(Server, Unreliable)
	void UpdateWeaponByCharacterMovementState_OnServer(FVector NewShootEndLocation, bool NewShouldReduceDispersion);
	UFUNCTION(NetMulticast, Unreliable)
	void AnimWeaponStart_MultiCast(UAnimMontage* Anim);
	UFUNCTION(NetMulticast, Unreliable)
	void FXWeaponFire_MultiCast(UParticleSystem* FxFire, USoundBase* SoundFire);

	UFUNCTION(NetMulticast, Unreliable)
	void SpawnTraceHitFX_Multicast(UParticleSystem* FxFire, FHitResult HitResult);
	UFUNCTION(NetMulticast, Unreliable)
	void SpawnTraceHitSound_Multicast(USoundBase* HitSound, FHitResult HitResult);
	UFUNCTION(NetMulticast, Unreliable)
	void SpawnTraceHitDecal_Multicast(UMaterialInterface* DecalMaterial, UPrimitiveComponent* OtherComp, FHitResult HitResult);
	UFUNCTION(NetMulticast, Unreliable)
	void AddEffectBySurface_Multicast(EPhysicalSurface mySurfacetype, TSubclassOf<UTPS_StateEffect> Effect, FHitResult HitResult);
	UFUNCTION(NetMulticast, Unreliable)
	void TraceDamage_Multicast(float ProjectileDamage, const FHitResult& HitResult);

	UFUNCTION(NetMulticast, Unreliable)
	void SpawnReloadSound_MultiCast(USoundBase* ReloadSound);


	UFUNCTION(Server, Unreliable)
	void SpawnLazer_OnServer(TSubclassOf<class ATPS_Lazer> myLazer);
	UFUNCTION(NetMulticast, Unreliable)
	void SpawnLazer_Multicast(TSubclassOf<class ATPS_Lazer> myLazer);
	UFUNCTION(NetMulticast, Reliable)
	void DestroyLazer_Multicast(ATPS_Lazer* myLazer);
};
