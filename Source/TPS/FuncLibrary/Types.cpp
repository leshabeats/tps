// Fill out your copyright notice in the Description page of Project Settings.


#include "Types.h"
#include "TPS/Interface/TPS_IGameActor.h"
#include "TPS/Interface/TPS_IGameActor.h"
#include "TPS/TPS.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/Character.h"

void UTypes::AddEffectBySurfaceType(AActor* TakeEffectActor, FName NameBoneHit, TSubclassOf<UTPS_StateEffect> AddEffectClass, EPhysicalSurface SurfaceType)
{
	if (SurfaceType != EPhysicalSurface::SurfaceType_Default && TakeEffectActor && AddEffectClass)
	{
		
		UTPS_StateEffect* myEffect = Cast<UTPS_StateEffect>(AddEffectClass->GetDefaultObject());
		if (myEffect)
		{
			bool bIsHavePossibleSurface = false;
			int8 i = 0;
			while (i < myEffect->PosibleInteractSurface.Num() && !bIsHavePossibleSurface)
			{
				if (myEffect->PosibleInteractSurface[i] == SurfaceType)
				{
					bIsHavePossibleSurface = true;
					bool bIsCanAddEffect = false;
					if (!myEffect->bIsStakable)
					{
						int8 j = 0;
						TArray<UTPS_StateEffect*> CurrentEffects;
						ITPS_IGameActor* myInterface = Cast<ITPS_IGameActor>(TakeEffectActor);
						if (myInterface)
						{
							CurrentEffects = myInterface->GetAllCurrentStateEffects();
						}

						if (CurrentEffects.Num() > 0)
						{
							while (j < CurrentEffects.Num() && !bIsCanAddEffect)
							{
								if (CurrentEffects[j]->GetClass() != AddEffectClass)
								{
									bIsCanAddEffect = true;
								}
								j++;
							}
						}
						else
						{
							bIsCanAddEffect = true;
						}
					}
					else
					{
						bIsCanAddEffect = true;
					}
					if (bIsCanAddEffect)
					{
						bIsHavePossibleSurface = true;
						UTPS_StateEffect* NewEffect = NewObject<UTPS_StateEffect>(TakeEffectActor, AddEffectClass);
						if (NewEffect)
						{
							NewEffect->InitObject(TakeEffectActor, NameBoneHit);
						}
					}
				}
				i++;
			}
		}
	}
}

void UTypes::ExecuteEffectAdded(UParticleSystem* ExecuteFX, AActor* Target, FVector Offset, FName Socket)
{
	if (Target)
	{
		FName SocketToAttach = Socket;
		FVector Loc = Offset;
		ACharacter* myCharacter = Cast<ACharacter>(Target);
		if(myCharacter && myCharacter->GetMesh())
		{
			UGameplayStatics::SpawnEmitterAttached(ExecuteFX, myCharacter->GetMesh(), SocketToAttach, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		}
		else
			if (Target->GetRootComponent())
			{
				UGameplayStatics::SpawnEmitterAttached(ExecuteFX, Target->GetRootComponent(), SocketToAttach, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
			}
	}
}
