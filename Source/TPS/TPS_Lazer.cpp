// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS_Lazer.h"

// Sets default values
ATPS_Lazer::ATPS_Lazer()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//SetReplicates(true);
	bReplicates = true;
}

// Called when the game starts or when spawned
void ATPS_Lazer::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATPS_Lazer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

